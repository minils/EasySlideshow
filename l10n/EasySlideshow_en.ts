<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>DetailsDialog</name>
    <message>
        <location filename="../ui/detailsdialog.ui" line="20"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../ui/detailsdialog.ui" line="61"/>
        <source>Filename</source>
        <translation>Filename</translation>
    </message>
    <message>
        <location filename="../ui/detailsdialog.ui" line="77"/>
        <source>Path</source>
        <translation>Path</translation>
    </message>
    <message>
        <location filename="../ui/detailsdialog.ui" line="93"/>
        <source>Size</source>
        <translation>Size</translation>
    </message>
    <message>
        <location filename="../ui/detailsdialog.ui" line="109"/>
        <source>Resolution</source>
        <translation>Resolution</translation>
    </message>
    <message>
        <location filename="../ui/detailsdialog.ui" line="119"/>
        <source>Creation Date</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DisplayLabel</name>
    <message>
        <location filename="../src/displaylabel.cpp" line="64"/>
        <source>Open &amp;folder</source>
        <translation>Open &amp;folder</translation>
    </message>
    <message>
        <location filename="../src/displaylabel.cpp" line="67"/>
        <source>Open &amp;image</source>
        <translation>Open &amp;image</translation>
    </message>
    <message>
        <location filename="../src/displaylabel.cpp" line="68"/>
        <source>Open image in the default viewer</source>
        <translation>Open image in the default viewer</translation>
    </message>
    <message>
        <location filename="../src/displaylabel.cpp" line="71"/>
        <source>&amp;Details</source>
        <translation>&amp;Details</translation>
    </message>
</context>
<context>
    <name>HelpDialog</name>
    <message>
        <location filename="../ui/helpdialog.ui" line="14"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../ui/helpdialog.ui" line="87"/>
        <source>&lt;img src=&quot;:/icon/app&quot; /&gt;
&lt;h1&gt;EasySlideshow v%1&lt;/h1&gt;
&lt;p&gt;
This is the first version of EasySlideshow.
&lt;/p&gt;
&lt;h2&gt;Help&lt;/h2&gt;
&lt;p&gt;
EasySlideshow takes one or multiple directories and shows the images inside of it in a randomized order. A &lt;b&gt;click&lt;/b&gt; on the image takes you to the subfolder where the image is located. Previous images can be accessed via the &lt;b&gt;[&amp;lt;]&lt;/b&gt; button.
The next image is shown in the set speed or by pressing the &lt;b&gt;[&amp;gt;]&lt;/b&gt; button.
Image location and display speed can be set in the settings dialog.
To pause the slideshow just press the &lt;b&gt;[II]&lt;/b&gt; button.
&lt;/p&gt;
&lt;h3&gt;Shortcuts&lt;/h3&gt;
The following shortcuts exist:
&lt;ul&gt;
&lt;li&gt;[Left Arrow] Go to the previous image&lt;/li&gt;
&lt;li&gt;[Right Arrow] Go the the next image&lt;/li&gt;
&lt;li&gt;[Space] Pause the slideshow&lt;/li&gt;
&lt;li&gt;[L] Rotate image left&lt;/li&gt;
&lt;li&gt;[R] Rotate image right&lt;/li&gt;
&lt;/ul&gt;
&lt;h2&gt;Source Code&lt;/h2&gt;
&lt;p&gt;This software is open source which can be found &lt;a href=&quot;https://github.com/minils/EasySlideshow&quot; style=&quot;color: #000;&quot;&gt;here&lt;/a&gt; on GitHub.&lt;/p&gt;
&lt;p&gt;Built from git commit &lt;b&gt;%2&lt;/b&gt;.&lt;p&gt;
&lt;h2&gt;License&lt;/h2&gt;
&lt;p&gt;This software is released under the &lt;b&gt;GNU General Public License 3.0&lt;/b&gt; which can be found &lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0.txt&quot; style=&quot;color: #000;&quot;&gt;here&lt;/a&gt;.&lt;/p&gt;
&lt;h2&gt;Author&lt;/h2&gt;
&lt;p&gt;Nils Schwabe &lt;a href=&quot;https://minils.de&quot; style=&quot;color: #000;&quot;&gt;www.minils.de&lt;/a&gt;&lt;/p&gt;
&lt;h2&gt;Icons&lt;/h2&gt;
&lt;p&gt;
Taken from &lt;b&gt;Material Icons&lt;/b&gt; which were released under the &lt;a href=&quot;https://creativecommons.org/licenses/by/4.0/&quot; style=&quot;color: #000;&quot;&gt;CC BY 4.0&lt;/a&gt; and can be found &lt;a href=&quot;https://design.google.com/icons/&quot; style=&quot;color: #000;&quot;&gt;here&lt;/a&gt;.
&lt;/p&gt;
&lt;h2&gt;Language Icons&lt;/h2&gt;
&lt;p&gt;
Taken from &lt;a href=&quot;http://www.famfamfam.com/lab/icons/flags/&quot; style=&quot;color: #000;&quot;&gt;FAMFAMFAM&lt;/a&gt;. &lt;i&gt;These flag icons are available for free use for any purpose with no requirement for attribution.&lt;/i&gt;
&lt;h2&gt;Roboto Font&lt;/h2&gt;
Released under the &lt;a href=&quot;http://www.apache.org/licenses/LICENSE-2.0.txt&quot;  style=&quot;color: #000;&quot;&gt;Apache License 2.0&lt;/a&gt;. Can be found &lt;a href=&quot;https://github.com/google/roboto/&quot; style=&quot;color: #000;&quot;&gt;here&lt;/a&gt;.
&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;h1&gt;EasySlideshow v%1&lt;/h1&gt;
&lt;p&gt;
This is the first version of EasySlideshow.
&lt;/p&gt;
&lt;h2&gt;Help&lt;/h2&gt;
&lt;p&gt;
EasySlideshow takes a folder and shows the images inside of it in a randomized order. A &lt;b&gt;click&lt;/b&gt; on the image takes you to the subfolder where the image is located. Previous images can be accessed via the &lt;b&gt;[&amp;lt;]&lt;/b&gt; button.
The next image is shown in the set speed or by pressing the &lt;b&gt;[&amp;gt;]&lt;/b&gt; button.
Image location and display speed can be set in the settings dialog.
To pause the slideshow just press the &lt;b&gt;[II]&lt;/b&gt; button.
&lt;/p&gt;
&lt;h2&gt;Source Code&lt;/h2&gt;
&lt;p&gt;This software is open source which can be found &lt;a href=&quot;https://github.com/minils/EasySlideshow&quot; style=&quot;color: #cccccc;&quot;&gt;here&lt;/a&gt; on GitHub.&lt;/p&gt;
&lt;h2&gt;License&lt;/h2&gt;
&lt;p&gt;This software is released under the &lt;b&gt;GNU General Public License 3.0&lt;/b&gt; which can be found &lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0.txt&quot; style=&quot;color: #cccccc;&quot;&gt;here&lt;/a&gt;.&lt;/p&gt;
&lt;h2&gt;Author&lt;/h2&gt;
&lt;p&gt;Nils Schwabe &lt;a href=&quot;https://minils.de&quot; style=&quot;color: #cccccc;&quot;&gt;www.minils.de&lt;/a&gt;&lt;/p&gt;
&lt;h2&gt;Icons&lt;/h2&gt;
&lt;p&gt;
Taken from &lt;b&gt;Material Icons&lt;/b&gt; which were released under the &lt;a href=&quot;https://creativecommons.org/licenses/by/4.0/&quot; style=&quot;color: #cccccc;&quot;&gt;CC BY 4.0&lt;/a&gt; and can be found &lt;a href=&quot;https://design.google.com/icons/&quot; style=&quot;color: #cccccc;&quot;&gt;here&lt;/&gt;.
&lt;/p&gt;</source>
        <translation type="vanished">&lt;h1&gt;EasySlideshow v%1&lt;/h1&gt;
&lt;p&gt;
This is the first version of EasySlideshow.
&lt;/p&gt;
&lt;h2&gt;Help&lt;/h2&gt;
&lt;p&gt;
EasySlideshow takes a folder and shows the images inside of it in a randomized order. A &lt;b&gt;click&lt;/b&gt; on the image takes you to the subfolder where the image is located. Previous images can be accessed via the &lt;b&gt;[&amp;lt;]&lt;/b&gt; button.
The next image is shown in the set speed or by pressing the &lt;b&gt;[&amp;gt;]&lt;/b&gt; button.
Image location and display speed can be set in the settings dialog.
To pause the slideshow just press the &lt;b&gt;[II]&lt;/b&gt; button.
&lt;/p&gt;
&lt;h2&gt;Source Code&lt;/h2&gt;
&lt;p&gt;This software is open source which can be found &lt;a href=&quot;https://github.com/minils/EasySlideshow&quot; style=&quot;color: #cccccc;&quot;&gt;here&lt;/a&gt; on GitHub.&lt;/p&gt;
&lt;h2&gt;License&lt;/h2&gt;
&lt;p&gt;This software is released under the &lt;b&gt;GNU General Public License 3.0&lt;/b&gt; which can be found &lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0.txt&quot; style=&quot;color: #cccccc;&quot;&gt;here&lt;/a&gt;.&lt;/p&gt;
&lt;h2&gt;Author&lt;/h2&gt;
&lt;p&gt;Nils Schwabe &lt;a href=&quot;https://minils.de&quot; style=&quot;color: #cccccc;&quot;&gt;www.minils.de&lt;/a&gt;&lt;/p&gt;
&lt;h2&gt;Icons&lt;/h2&gt;
&lt;p&gt;
Taken from &lt;b&gt;Material Icons&lt;/b&gt; which were released under the &lt;a href=&quot;https://creativecommons.org/licenses/by/4.0/&quot; style=&quot;color: #cccccc;&quot;&gt;CC BY 4.0&lt;/a&gt; and can be found &lt;a href=&quot;https://design.google.com/icons/&quot; style=&quot;color: #cccccc;&quot;&gt;here&lt;/&gt;.
&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.ui" line="14"/>
        <source>EasySlideshow</source>
        <translation>EasySlideshow</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="174"/>
        <source>Loading...</source>
        <translation>Loading...</translation>
    </message>
    <message>
        <source>s</source>
        <translation type="vanished">s</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="224"/>
        <source>Changed language to &lt;b&gt;%1&lt;/b&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="241"/>
        <source>Scanning folders...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../ui/settingsdialog.ui" line="26"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../ui/settingsdialog.ui" line="100"/>
        <source>Image path:</source>
        <translation>Image path:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="187"/>
        <source>Browse...</source>
        <translation>Browse...</translation>
    </message>
    <message>
        <location filename="../ui/settingsdialog.ui" line="136"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settingsdialog.ui" line="161"/>
        <source>Duration:</source>
        <translation>Duration:</translation>
    </message>
    <message>
        <location filename="../ui/settingsdialog.ui" line="216"/>
        <source> s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settingsdialog.ui" line="241"/>
        <source>Click on Image:</source>
        <translation>Click on Image:</translation>
    </message>
    <message>
        <location filename="../ui/settingsdialog.ui" line="271"/>
        <source>open fo&amp;lder</source>
        <translation>open fo&amp;lder</translation>
    </message>
    <message>
        <location filename="../ui/settingsdialog.ui" line="287"/>
        <source>&amp;pause</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settingsdialog.ui" line="303"/>
        <source>&amp;nothing</source>
        <translation></translation>
    </message>
    <message>
        <source>pause</source>
        <translation type="vanished">pause</translation>
    </message>
    <message>
        <source>nothing</source>
        <translation type="vanished">nothing</translation>
    </message>
    <message>
        <source>s</source>
        <translation type="vanished">s</translation>
    </message>
    <message>
        <location filename="../ui/settingsdialog.ui" line="315"/>
        <source>Language</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/settingsdialog.ui" line="358"/>
        <source>ERROR</source>
        <translation>ERROR</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="15"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="16"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="173"/>
        <source>Cannot add more than %1 paths</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="212"/>
        <source>Pick a folder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="223"/>
        <source>At least one path is needed</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SlideShow</name>
    <message>
        <location filename="../src/slideshow.cpp" line="91"/>
        <source>Did not find any images</source>
        <translation></translation>
    </message>
</context>
</TS>
